
document.getElementById("debt-form").addEventListener("submit", function(e) {
	  // Hide Results
  document.getElementById("result").style.display = "none";
 
  // Show Loader
  document.getElementById("loading").style.display = "block";
  // Hide Results
  setTimeout(calculateResults, 1000);
 
  e.preventDefault();
});
 
// Calculate Results
function calculateResults() {
 
  const monthlySalary = document.getElementById("monthlySalary");
  const monthlyCommitment = document.getElementById("monthlyCommitment");
  const totalBorrow = document.getElementById("totalBorrow");

 
  //Computed Monthly payment
  
  const totalSalary = parseFloat(monthlySalary.value);
  const totalCommitment = parseFloat(monthlyCommitment.value);
  const calculateBorrow = (totalSalary - totalCommitment);
 
 
  if (isFinite(calculateBorrow)) {
    totalBorrow.value = (calculateBorrow * 5).toFixed(2);
 
    // Show Results
    document.getElementById("result").style.display = "block";
 
    // Hide Loader
    document.getElementById("loading").style.display = "none";
  } else {
    showError("Please check number inputs");
  }
}

// Show Error
function showError(error) {
  // Hide Results
  document.getElementById("result").style.display = "none";

  // Hide Loader
  document.getElementById("loading").style.display = "none";

  // Create a div
  const errorDiv = document.createElement("div");

  // Get Elements
  const card = document.querySelector(".card");
  const heading = document.querySelector(".heading");

  // Add class
  errorDiv.className = "alert alert-danger";

  // Create text node and append div
  errorDiv.appendChild(document.createTextNode(error));

  // Insert error above heading
  card.insertBefore(errorDiv, heading);

  // Clear Error after 3 seconds
  setTimeout(clearError, 3000);

  // Clear Error
  function clearError() {
    document.querySelector(".alert").remove();
  }
}
 